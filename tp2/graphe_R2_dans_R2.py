#!/bin/python3

import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt



def f(a,b):
    #return a**2 + 2*a*b - 1
    return (a**3)*b - 3*(a**2)*(b-1)+(b**2)-1


def g(a,b):
    #return a**2*b**2 - b - 1
    return (a**2)*(b**2)-2

def praparation_graphe():
    fig = plt.figure(figsize = (20,20))
    ax = fig.add_subplot(1, 1, 1, projection='3d')

    ax.set_xlabel('$a$', labelpad=20)
    ax.set_ylabel('$b$', labelpad=20)
    ax.set_zlabel('$f(a,b)$ et $g(a,b)$', labelpad=20)

    xplot = np.arange (-2, 1.7, 0.1)
    yplot = np.arange (-1.5, 0.8, 0.1)
    return xplot,yplot,ax


##### Graphe de f #####

def graphe_f():
    xplot, yplot,ax = praparation_graphe()
    X, Y = np.meshgrid (xplot, yplot)
    Z = f(X,Y)

    ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
    ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
    ax.contour(X, Y, Z, 0, colors="black",  levels=np.array([0], dtype=np.float64), linestyles="solid")
    plt.show()
    
##### Graphe de g #####
def graphe_g():
    xplot, yplot,ax = praparation_graphe()
    X, Y = np.meshgrid (xplot, yplot)
    Z = g(X,Y)

    ax.plot_surface(X, Y, Z, cmap="autumn_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
    ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
    ax.contour(X, Y, Z, 0, colors="black",  levels=np.array([0], dtype=np.float64), linestyles="solid")
    plt.show()

# l'affichage de graphe des f et g 
#graphe_f()
#graphe_g()









